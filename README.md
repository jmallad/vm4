This project consists of a 64-bit CISC virtual machine with paged memory,
interrupts, and floating point instructions. Alongside this project, I also
developed a simple executable file format (vxf). This was a very educational
project for me. I learned about many fundamentals of operating systems and CPU
design, and got my hands dirty with many of the tradeoffs made when designing
an instruction set.

My initial goal with this particular project was improving on the decoding
stage of my last emulator project, which used a large switch case for each
individual instruction. I am proud of how I accomplished this here, and you
can see the work in src/cpu/decode.c. There is plenty of documentation in the
doc/ folder as well. 

