#pragma once

#include "cpu/machine.h"
#include "types.h"

int io_write(struct machine* m, u8 out, void* in, u8 size);
int io_read(struct machine* m, u8 in, void* out, u8 size);


