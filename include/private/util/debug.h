#pragma once

#include "types.h"
#include "cpu/instruction.h"

void dump_arg(char* out, size_t outlen, unsigned char type, u64 arg);
void dump_instr(struct instruction* in);
void dump_machine(struct machine* m);
