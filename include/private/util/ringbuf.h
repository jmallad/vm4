#pragma once
#include "types.h"

/* Define which type is stored as data in a single element of a ring buffer. */
typedef u8 ringbuf_data;

/* This struct represents a single item of a ringbuffer. */
struct ring_item {
	ringbuf_data data;
	/* This flag is nonzero when this item contains data that has yet
	   to be read */
	unsigned char state;
};

/* This struct represents a single ringbuffer. */
struct ring {
	struct ring_item* items;
	/* Number of items in the item array */
	unsigned n_items;
	/* Points to which item will be inserted next */
	unsigned head;
	/* Points to which item will be popped next */
	unsigned tail;
	/* If this value is nonzero, ringbuffer insertion will always succeed, 
	   even if this means overwriting data that has yet to be read. 
	   When this value is zero, ringbuf_insert will return EFULL when
	   the buffer is full. */
	unsigned char overwrite;
};

/* Allocate and zero a new ringbuf structure with n_items elements 
   Errors:
   ENULLPTR - `p' parameter was NULL
   EALLOC   - `malloc' returned NULL while allocating space for the buffer */
int ringbuf_new(void* p, unsigned n_items, unsigned char overwrite);

/* Free a ringbuf structure */
void ringbuf_free(struct ring* r);

/* Insert an item into a ring buffer 
   Errors:
   ENULLPTR - `r' parameter or its `items' pointer was NULL
   EFULL    - `overwrite' is true and buffer is full */
int ringbuf_push(struct ring* r, ringbuf_data data);

/* Pop an item from a ring buffer 
   Errors:
   ENULLPTR - `r' parameter or its `items' pointer was NULL 
   EEMPTY   - no data is available to read from the buffer */
int ringbuf_pop(struct ring* r, ringbuf_data* data);

/* Push a sequence of items to a ring buffer 
   Returns:
   Amount of bytes successfully pushed to the buffer, or a negative error code
   Errors:
   ENULLPTR - `r' parameter or `input' parameter was NULL 
   EEMPTY   - `n_input' parameter was zero */
int ringbuf_push_seq(struct ring* r, ringbuf_data* input, unsigned n_input);

/* Pop a sequence of items from a ring buffer 
   Returns:
   Amount of bytes successfully popped to the output, or a negative error code
   Errors:
   ENULLPTR - `r' parameter or `output' parameter was NULL 
   EFULL    - `n_output' parameter was zero */
int ringbuf_pop_seq(struct ring* r, ringbuf_data* output, unsigned n_output);

