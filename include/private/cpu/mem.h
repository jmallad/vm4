#pragma once

#include "types.h"

/* Address constants */
enum {
	REGMAPE = 127,       /* end address of mapped GPRs */
	/* addresses of timer 0 and 1 registers */
	T0B = 128,
	T1B = 136,
	ICRB = 144,          /* address of interrupt control register */
	ICRE = 147,          /* end address of ICR */
	TCRB = 148,          /* address of timer control register */
	CPUB = 149,          /* address of CPU frequency register */
	IVTB  = 157,         /* address of interrupt vector table */
	IVTE  = 277,         /* end address of IVT */
	UKMB  = 278,         /* address of UKM register */
	TABLEB = 279,        /* address of Page Table register */
	SREGMAPE = 286,      /* end address of special registers */
	IOB   = 295,         /* start of I/O buffers */
	IOCB  = 0x10006f,    /* start of I/O control structures */
	DATAB  = 0x1018aef,  /* start of usable data area */
};
/* I/O-related constants */
enum {
	IOBLEN = 65536, /* Length of I/O buffer */
	IOCBLEN = 4,    /* Length of I/O control structure */
};
/* Copy directions as passed to vmemcpy() */
enum {
	TO_HOST = 0, /* Copy from virtual to physical memory */
	TO_VM   = 1  /* Copy from physical to virtual memory */
};
/* Addressing modes as passed to vmemcpy() */
enum {
	AUTO = 0,   /* Select based on UKM; LINEAR if KERNEL, else PAGED */
	LINEAR = 1, /* Linear (fixed) address space, I/O and data area only */
	PAGED = 2   /* Paged (virtualized) address space, data area only */
};
/* Page flags */
enum {
	/* Permission modes */
	MODE_READ = 0,
	MODE_WRITE = 1,
	MODE_EXECUTE = 2,
	/* Bits 3-7 reserved for OS usage */
	MODE_MAX = 8
};

/* Address width in bits */
#define ADDRESS_WIDTH    (40)
/* Page length in bytes */
#define DEFAULT_PAGELEN  (0x400000)
/* Maximum number of pages to allocate */
#define DEFAULT_MAXALLOC (DEFAULT_NPAGES)

/* All following values are calculated from the above constants */
#define DEFAULT_ASLEN    (1ULL << ADDRESS_WIDTH)
#define DEFAULT_NPAGES   (DEFAULT_ASLEN / DEFAULT_PAGELEN)
/* Page table offsets */
#define PTBLB (DEFAULT_NPAGES)      /* start of page table */
#define PTBLE (PTBLB * 5)           /* end of page table */
#define MPMAPB (0)                  /* start of memory permission map */
#define	MPMAPE (PTBLB-1)            /* end of memory permission map */

struct vram {
       size_t aslen;     /* Size of a single address space in bytes */
       unsigned pagelen;   /* Size of a single page in bytes */
       unsigned npages;    /* Number of pages per address space */
       unsigned maxalloc;  /* Maximum number of pages to allocate */
       unsigned nalloc;    /* Current number of allocated pages */
/******* Layout of two-level address translation table ******

                        u8** ram [npages == 2]
                               |
 		u8* ram[0]----------u8* ram[1] :::: Frame
		          |               |
 	     u8 ram[0][0]---...          ...   :::: Offset

The top level of the table is empty for a new VM. Frames are allocated 
on-demand, during program runtime. Frames may not be freed. In user mode,
each frame is associated with a page as according to a Page Table (see
doc/architecture.txt "Page Table"). 
******/
	u8** ram;
};

/* Forward declaration */
struct machine;

int mem_check(struct machine* m, u64 virt, u8 mode);
int vmemcpy(struct machine* m, u64 virt, void* phys1, size_t len, u8 dir,
	u8 am);

