
#pragma once
#include "cpu/machine.h"

enum {
	/*** Interrupts ***/
	/* Data Ready channels 0 - 15 */
	IDR0  = 0,   
	IDR1  = 1,   
	IDR2  = 2,   
	IDR3  = 3,   
	IDR4  = 4,   
	IDR5  = 5,   
	IDR6  = 6,   
	IDR7  = 7,   
	IDR8  = 8,   
	IDR9  = 9,   
	IDR10 = 10, 
	IDR11 = 11, 
	IDR12 = 12, 
	IDR13 = 13,
	IDR14 = 14,
	IDR15 = 15,
	IT0   = 16, /* Timer 0 Triggered */
	IT1   = 17, /* Timer 1 Triggered */
	/* Start of non-maskable interrupts */
	ISYS  = 18, /* Syscall */
	IINST = 19, /* Invalid instruction */
	INULL = 20, /* Null pointer dereference */
	IADDR = 21, /* Invalid address */
	IDIVZ = 22, /* Division by zero */
	IGPROT = 23, /* General Protection fault */
	IPFAULT = 24, /* Page fault */
	IMAX  = 25,
};

int interrupt_push(struct machine* m, u8 id);
int interrupt_handle(struct machine* m);
int error_handle(struct machine* m, int err);
