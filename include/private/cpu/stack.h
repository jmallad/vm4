#pragma once

#include "cpu/machine.h"

int machine_push(struct machine* m, void* in, size_t size);
int machine_pop(struct machine* m, void* out, size_t size);

