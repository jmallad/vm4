#pragma once

#include "cpu/machine.h"
#include "types.h"

int fetch(struct machine* m, void* out, size_t len);
int sleep_handle(struct machine* m);
int step(struct machine* m);
