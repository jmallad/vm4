#pragma once

#include "cpu/machine.h"

/* Timer control register flags */
enum {
	T0EN = 0, /* Timer 0 enabled */
	T1EN = 1, /* Timer 1 enabled */
	TMAX = 2
};

/* Enable/disable timers */
int timer_enable(struct machine* m, u8 id);
int timer_disable(struct machine* m, u8 id);
/* Check if timers need to be triggered and push their interrupts into the
   queue if so */
int timer_checks(struct machine* m);

