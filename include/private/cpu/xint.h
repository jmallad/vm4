#pragma once

#include "types.h"
#include "cpu/machine.h"

typedef struct {
	i8 size;
	union {
		u8 x8;
		i8 s8;
		u16 x16;
		i16 s16;
		u32 x32;
		i64 s32;
		u64 x64;
		i64 s64;
		float f32;
		double f64;
	};
} xint;

int valid_size(i8 in);
void swap_xint(xint* in);
int compare_xint(xint* x, xint* y, u8 sign);
int op_xint(xint* x, xint* y, u8 op, u8 sign);
int store_xint(xint* out, void* in, i8 size, bool swap);
int load_xint(void* out, xint* in, bool swap);
int vload_xint(struct machine* m, u32 out, xint* in);
int vstore_xint(struct machine* m, xint* out, u32 in, i8 size);
