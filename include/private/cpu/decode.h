#pragma once

#include "cpu/execute.h"

int decode(struct machine* m, executor* e, struct instruction* out,
	   u64 in);
