#pragma once

struct compiler {
	gcc_jit_context* ctx;
	gcc_jit_type u8_type;
	gcc_jit_type u16_type;
	gcc_jit_type u32_type;
	gcc_jit_type u64_type;
	gcc_jit_type i8_type;
	gcc_jit_type i16_type;
	gcc_jit_type i32_type;
	gcc_jit_type i64_type;
	
};
