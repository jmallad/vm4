#pragma once

#include "types.h"

/* Opcodes */
enum {
	/* Arithmetic */
	ADD = 0,
	SUB = 1,
	MUL = 2,
	DIV = 3,
	NEG = 4,
	MOD = 5,
	/* Bitwise */
	SHB = 6,
	ROB = 7,
	AND = 8,
	OR  = 9,
	XOR = 10,
	NOT = 11,
	/* Move and compare */
	MOV = 12,
	CMP = 13,
	/* Interrupt */
	INT = 14,
	/* Floating point */
	FADD = 15,
	FSUB = 16,
	FMUL = 17,
	FDIV = 18,
	FMOD = 19,
	FCHS = 20,
	FCMP = 21,
	/* Function entry/exit and stack operations */
	CALL = 22,
	RET  = 23,
	PUSH = 24,
	POP  = 25,
	OPMAX = 26
};

/* Argument types */
enum {
	NONE = 0, /* No argument supplied */
	REG = 1,  /* Register */
	MEM = 2,  /* Value at address pointed to by immediate value */
	PTR = 3,  /* Value at address pointed to by register */
	IMM = 4,  /* Immediate value */
        OFF = 5   /* Value of register plus offset */
};

/* Condition codes */
enum {
	NC = 0,  /* None */
	ZC = 1,  /* Zero */
	EC = 2,  /* Equal */
	NE = 3,  /* Not equal */
	LC = 4,  /* Lesser than */
	LEC = 5, /* Lesser than or equal to */       	
	GC = 6,  /* Greater than */
	GEC = 7  /* Greater than or equal to */
};

/* Register constants */
enum {
	R0 = 0,
	R1 = 1,
	R2 = 2,
	R3 = 3,
	R4 = 4,
	R5 = 5,
	R6 = 6,
	R7 = 7,
	R8 = 8,
	R9 = 9,
	R10 = 10,
	R11 = 11,
	R12 = 12,
	R13 = 13,
	RIP = 14,
	RSP = 15
};

#define OP_MASK   (~0xffffffffffffffe0)
#define CC_MASK   (~0xffffffffffffff1f) 
#define R1_MASK   (~0xfffffffffffff0ff)
#define R2_MASK   (~0xffffffffffff0fff)
#define SIGN_MASK (~0xfffffffffffeffff)
#define SIZE_MASK (~0xfffffffffff9ffff)
#define OFF_MASK  (~0xfffffff80007ffff)
#define DST_MASK  (0x1800000000)
#define SRC_MASK  (0xe000000000)

/* Return mask for `_up' amount of upper bits from a `_bits' wide value */
#define UMASK(_up, _bits) (((1UL << _up) - 1) << (_bits - _up))
/* Return mask for `_low' amount of lower bits from any width value */
#define LMASK(_low) ((1UL << _low) - 1)

struct instruction {
	u8 op;
	u8 cc;
	u8 r1;
	u8 r2;
	u8 sign;
	u8 size;
	i16 off;
	u64 imm;
	unsigned char arg1_type;
	unsigned char arg2_type;
	u64 arg1;
	u64 arg2;
};
