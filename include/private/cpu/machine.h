#pragma once

#include "types.h"
#include "cpu/mem.h"

/* Return codes */
enum {
	LIMITED  = 2,    /* Step limited */
	HALTED   = 1,    /* Normal termination */
	SUCCESS  = 0,    /* No error */
	ENULLPTR = -1,   /* Null pointer dereference */
	EALLOC   = -2,   /* Dynamic allocation failure */
	EINVINST = -3,   /* Invalid instruction */
	EINVTARGET = -4, /* Invalid target */
	EINVSIZE = -5,   /* Invalid size specified */
	EINVARG = -6,    /* Invalid argument number specified */
	EINVADDR = -7,   /* Invalid address specified */
	ENOTFOUND = -8,  /* Page not found */
	EREAD   = -11,   /* Failed to read file */
	EASM    = -12,   /* Invalid assembly */
	EINVSTREAM = -13, /* Invalid stream type */
	EMODE = -14,     /* I/O Permission failure */
	EDIVZERO = -15,  /* Division by zero */
	EBUFOV = -16,    /* I/O Buffer overflow */
	EINVSP = -17,    /* Invalid stack pointer */
	EEMPTY = -18,    /* Buffer empty */
	EFULL  = -19,    /* Buffer full */
	ETIMESPEC = -20, /* Time library error */
	EINVINT = -21,   /* Invalid interrupt vector */
	EINVCHAN = -22,  /* Invalid I/O channel specified */
	EGPROT = -23,    /* General Protection fault */
	EPFAULT = -24,   /* Page Fault */
};

/* Machine states */
enum {
	STOPPED = 0,
	INTERRUPTED = 1,
	RUNNING = 2
};

/* Default values */
enum {
	DEFAULT_INTQUEUE = 8 /* Number of slots in the interrupt queue */
};

/* Comparison result flags */
enum {
	ZF = 0,  /* Zero */
	EF = 1,  /* Equal */
	GF = 2,  /* Greater than */
	LF = 3,  /* Lesser than */
	AF = 4,  /* Unsigned GT */
	BF = 5,  /* Unsigned LT */
	NF = 6,  /* Not equal */
};

/* Mode flags */
enum {
	KERNEL = 0,
	USER = 1,
	MODEMAX = 2
};

struct machine {	
	/* Memory */
	struct vram memory; 
	/* CPU */
	size_t limit;  /* Step limit */
	size_t step;   /* Step counter */
	u64 reg[16];   /* R0-R13, IP, SP */
	u8 result;     /* Result of last comparison */
	u8 state;      /* Current operating state of machine */
	/* Mapped registers are all 64-bit to prevent overflows */
	u64 freq;      /* CPU frequency in Hz */
	u64 icr;       /* Interrupt Control Register */
	u64 tcr;       /* Timer Control Register */
	u64 tv0;       /* Timer value 0 */
	u64 tv1;       /* Timer value 1 */
	u64 ukm;       /* User/Kernel Mode */
	u64 table;     /* Virtual address of page table */
	/* Interrupts and I/O */
	u8 maxints;    /* Maximum number of interrupts in the queue */
	u8 nints;      /* Number of interrupts which need processing */
	struct ring* intbuf; /* Interrupts which need processing */
	/* Run-time properties */
	u8 swap;       /* Nonzero when loaded program has a different 
			  endianness from the host system */
	u8 debug; /* When set, debug messages are logged to stderr */
};
/******************************
       Global functions
******************************/
/* Machine creation, configuration, and destruction */
int machine_new(struct machine** out);
void machine_free(struct machine* m);
void machine_setlimit(struct machine* m, size_t limit);
void machine_setfreq(struct machine* m, u64 hz);
void machine_setmaxalloc(struct machine* m, unsigned maxalloc);
int machine_setmaxints(struct machine* m, u8 maxints);
void machine_setmode(struct machine* m, u8 mode);
void machine_setdebug(struct machine* m, u8 debug);
int machine_getstate(struct machine* m);
/* Load program */
int machine_load(struct machine* out, void* in, u64 inlen, u8 endian,
		 u64 load, u64 start);
/* Running machines */
int machine_run_nofreq(struct machine* m);
int machine_run(struct machine* m);
/* Error code interpretation */
const char* vm_strerror(int ret);
/******************************
       Local functions
******************************/
/* Context save/restore */
int machine_save(struct machine* m);
int machine_restore(struct machine* m);
