#pragma once

#include "cpu/machine.h"
#include "cpu/instruction.h"

typedef int (*executor)(struct machine*, struct instruction*);

int execute_math_two(struct machine* m, struct instruction* in);
int execute_math_one(struct machine* m, struct instruction* in);
int execute_mem_two(struct machine* m, struct instruction* in);
int execute_cmp_two(struct machine* m, struct instruction* in);
int execute_cmp_one(struct machine* m, struct instruction* in);
int execute_call_one(struct machine* m, struct instruction* in);
int execute_ret_one(struct machine* m, struct instruction* in);
int execute_push_one(struct machine* m, struct instruction* in);
int execute_pop_one(struct machine* m, struct instruction* in);
int execute_int_one(struct machine* m, struct instruction* in);
