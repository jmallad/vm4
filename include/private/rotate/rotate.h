#pragma once

#include "types.h"

u64 ror64_native(u64 x, u8 y);
u32 ror32_native(u32 x, u8 y);
u16 ror16_native(u16 x, u8 y);
u8 ror8_native(u8 x, u8 y);


