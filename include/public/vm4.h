#pragma once

#include <stdint.h>

/* Return codes */
enum {
	LIMITED  = 2,    /* Step limited */
	HALTED   = 1,    /* Normal termination */
	SUCCESS  = 0,    /* No error */
	ENULLPTR = -1,   /* Null pointer dereference */
	EALLOC   = -2,   /* Dynamic allocation failure */
	EINVINST = -3,   /* Invalid instruction */
	EINVTARGET = -4, /* Invalid target */
	EINVSIZE = -5,   /* Invalid size specified */
	EINVARG = -6,    /* Invalid argument number specified */
	EINVADDR = -7,   /* Invalid address specified */
	ENOTFOUND = -8,  /* Page not found */
	EREAD   = -11,   /* Failed to read file */
	EASM    = -12,   /* Invalid assembly */
	EINVSTREAM = -13, /* Invalid stream type */
	EMODE = -14,     /* I/O Permission failure */
	EDIVZERO = -15,  /* Division by zero */
	EBUFOV = -16,    /* I/O Buffer overflow */
	EINVSP = -17,    /* Invalid stack pointer */
	EEMPTY = -18,    /* Buffer empty */
	EFULL  = -19,    /* Buffer full */
	ETIMESPEC = -20, /* Time library error */
	EINVINT = -21,   /* Invalid interrupt vector */
	EINVCHAN = -22,  /* Invalid I/O channel specified */
	EGPROT = -23,    /* General Protection fault */
	EPFAULT = -24,   /* Page Fault */
};

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

struct machine;

/* Machine creation, configuration, and destruction */
int machine_new(struct machine** out);
void machine_free(struct machine* m);
void machine_setlimit(struct machine* m, size_t limit);
void machine_setfreq(struct machine* m, u64 hz);
void machine_setmaxalloc(struct machine* m, unsigned maxalloc);
int machine_setmaxints(struct machine* m, u8 maxints);
void machine_setmode(struct machine* m, u8 mode);
void machine_setdebug(struct machine* m, u8 debug);
int machine_getstate(struct machine* m);
/* Load program */
int machine_load(struct machine* out, void* in, u64 inlen, u8 endian,
		 u64 load, u64 start);
/* Running machines */
int machine_run_nofreq(struct machine* m);
int machine_run(struct machine* m);
/* I/O */
int io_write(struct machine* m, u8 out, void* in, u8 size);
int io_read(struct machine* m, u8 in, void* out, u8 size);
/* Error code interpretation */
const char* vm_strerror(int ret);

