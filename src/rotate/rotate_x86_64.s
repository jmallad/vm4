	.global ror64_native
	.global ror32_native
	.global ror16_native
	.global ror8_native

	.text
	
ror64_native:
	rorq %rax
	ret
ror32_native:
	rorl %eax
	ret
ror16_native:
	rorw %ax
	ret
ror8_native:
	rorb %al
	ret

