#include "types.h"
#include "cpu/xint.h"

u64 ror64_native(u64 x, u8 y)
{
	unsigned i;
	unsigned msb;
	for (i = 0; i < y; i++) {
		msb = x >> 63;
		x = (x << 1) | msb;
	}
	return x;
}

u32 ror32_native(u32 x, u8 y)
{
	unsigned i;
	unsigned msb;
	for (i = 0; i < y; i++) {
		msb = x >> 31;
		x = (x << 1) | msb;
	}
	return x;
}

u16 ror16_native(u16 x, u8 y)
{
	unsigned i;
	unsigned msb;
	for (i = 0; i < y; i++) {
		msb = x >> 15;
		x = (x << 1) | msb;
	}
	return x;
}

u8 ror8_native(u8 x, u8 y)
{
	unsigned i;
	unsigned msb;
	for (i = 0; i < y; i++) {
		msb = x >> 7;
		x = (x << 1) | msb;
	}
	x = y;
	return x;
}
