#include <stdlib.h>
#include <string.h>
#include "util/ringbuf.h"
#include "cpu/machine.h"

/* Allocate and zero a new ringbuf structure with n_items elements */
int ringbuf_new(void* out, unsigned n_items, unsigned char overwrite)
{
	struct ring** p;
	if (!out) {
		return ENULLPTR;
	}
	p = out;
	*p = malloc(sizeof(struct ring));
	if (!*p) {
		return EALLOC;
	}
	memset(*p, 0, sizeof(struct ring));
	(*p)->items = malloc(sizeof(struct ring_item) * n_items);
	if (!(*p)->items) {
		free(*p);
		return EALLOC;
	}
	memset((*p)->items, 0, sizeof(struct ring_item) * n_items);
	(*p)->n_items = n_items;
	(*p)->overwrite = overwrite;
	return SUCCESS;
}

/* Free a ringbuf structure */
void ringbuf_free(struct ring* r)	
{
	if (!r) {
		return;
	}
	if (r->items) {
		free(r->items);
	}
	free(r);
}

/* Insert an item into a ring buffer */
int ringbuf_push(struct ring* r, ringbuf_data data)
{
	if (!r) {
		return ENULLPTR;
	}
	if (!r->items || !r->n_items) {
		return ENULLPTR;
	}
	if (r->head == r->tail && r->items[r->head].state) {
		if (!r->overwrite) {				
			return EFULL;
		}
		if (r->tail + 1 > r->n_items - 1) {
			r->tail = 0;
		}
		else {
			r->tail++;
		}
	}
	r->items[r->head].data = data;
	r->items[r->head].state = 1;
	if (r->head == r->n_items - 1) {
		r->head = 0;
	}
	else {
		r->head++;
	}
	return SUCCESS;
}

/* Pop an item from a ring buffer */
int ringbuf_pop(struct ring* r, ringbuf_data* data)
{
	if (!r || !data) {
		return ENULLPTR;
	}
	if (!r->items || !r->n_items) {
		return ENULLPTR;
	}
	if (!r->items[r->tail].state) {
		return EEMPTY;
	}
	*data = r->items[r->tail].data;
	r->items[r->tail].data = 0;
	r->items[r->tail].state = 0;
	if (r->tail == r->n_items - 1) {
		r->tail = 0;
	}
	else {
		r->tail++;
	}
	return SUCCESS;
}

/* Push a sequence of items to a ring buffer */
int ringbuf_push_seq(struct ring* r, ringbuf_data* input, unsigned n_input)
{
	unsigned i;
	unsigned c;
	int ret;
	if (!r || !input) {
		return ENULLPTR;
	}
	if (!n_input) {
		return EEMPTY;
	}
	if (n_input > r->n_items) {
		c = r->n_items;
	}
	else {
		c = n_input;
	}
	for (i = 0; i < c; i++) {
		ret = ringbuf_push(r, input[i]);
		if (ret) {
			if (ret == EFULL) {
				break;
			}
			return ret;
		}		
	}
	return i;
}

/* Pop a sequence of items from a ring buffer */
int ringbuf_pop_seq(struct ring* r, ringbuf_data* output, unsigned n_output)
{
	unsigned i;
	unsigned c;
	int ret;
	if (!r || !output) {
		return ENULLPTR;
	}
	if (!n_output) {
		return EFULL;
	}
	if (n_output > r->n_items) {
		c = r->n_items;
	}
	else {
		c = n_output;
	}
	for (i = 0; i < c; i++) {
		ret = ringbuf_pop(r, &output[i]);
		if (ret) {
			if (ret == EEMPTY) {
				break;
			}
			return ret;
		}
	}
	return i;
}

