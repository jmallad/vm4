#include <stdio.h>
#include <inttypes.h>
#include "cpu/instruction.h"
#include "cpu/machine.h"

static char* dump_op(struct instruction* in)
{
	switch (in->op) {
	case ADD:
		return "add";
	case SUB:
		return "sub";
	case MUL:
		return "mul";
	case DIV:
		return "div";
	case NEG:
		return "neg";
	case MOD:
		return "mod";
	case SHB:
		if (in->sign) {
			return "shl";
		}
		else {
			return "shr";
		}
	case ROB:
		if (in->sign) {
			return "rol";
		}
		else {
			return "ror";
		}
	case AND:
		return "and";
	case OR:
		return "or";
	case XOR:
		return "xor";
	case MOV:
		return "mov";
	case CMP:
		return "cmp";
	case NOT:
		return "not";
	case INT:
		return "int";
	case FADD:
		return "fadd";
	case FSUB:
		return "fsub";
	case FMUL:
		return "fmul";
	case FDIV:
		return "fdiv";
	case FMOD:
		return "fmod";
	case FCHS:
		return "fchs";
	case FCMP:
		return "fcmp";
	case CALL:
		return "call";
	case RET:
		return "ret";
	case PUSH:
		return "push";
	case POP:
		return "pop";
	}
	return "(unknown)";
}

static char* dump_cc(u8 cc)
{
	switch(cc) {
	case NC:
		return "none";
	case ZC:
		return "zero";
	case EC:
		return "equal";
	case GC:
		return "greater";
	case LC:
		return "lesser";
	case GEC:
		return "greater or equal to";
	case LEC:
		return "lesser or equal to";
	}
	return "(unknown)";
}

static char* dump_type(u8 type)
{
	switch (type) {
	case NONE:
		return "none";
	case REG:
		return "register";
	case MEM:
		return "address";
	case PTR:
		return "pointer";
	case IMM:
		return "immediate";
	case OFF:
		return "register + offset";
	}
	return "(unknown)";			
}

static char* dump_reg(u64 reg)
{
	switch (reg) {
	case RIP:
		return "RIP";
	case RSP:
		return "RSP";
	case R0:
		return "R0";
	case R1:
		return "R1";
	case R2:
		return "R2";
	case R3:
		return "R3";
	case R4:
		return "R4";
	case R5:
		return "R5";
	case R6:
		return "R6";
	case R7:
		return "R7";
	case R8:
		return "R8";
	case R9:
		return "R9";
	case R10:
		return "R9";
	case R11:
		return "R11";
	case R12:
		return "R12";
	case R13:
		return "R13";
	}
	return "??";
}

void dump_arg(char* out, size_t outlen, unsigned char type, u64 arg)
{
	switch(type) {
	case NONE:
		snprintf(out, outlen, "none");
		return;
	case REG:
		snprintf(out, outlen, "%s", dump_reg(arg));
		return;
	case MEM:
		snprintf(out, outlen, "[0x%lux]", arg);
		return;
	case PTR:
		snprintf(out, outlen, "[%s]", dump_reg(arg));
		return;
	case IMM:
		snprintf(out, outlen, "%lu", arg);
		return;
	case OFF:
		snprintf(out, outlen, "%s + offset", dump_reg(arg));
		return;
	}
	snprintf(out, outlen, "???");
}

void dump_instr(struct instruction* in)
{
	if (!in) {
		return;
	}
	char* op = dump_op(in);
	char* cc = dump_cc(in->cc);
	char* type1 = dump_type(in->arg1_type);
	char* type2 = dump_type(in->arg2_type);
	char arg1[16] = {0};
	char arg2[16] = {0};
	dump_arg(arg1, 16, in->arg1_type, in->arg1);
	dump_arg(arg2, 16, in->arg2_type, in->arg2);
	
	fprintf(stderr,
		"OP: %s\nCC: %s\nTYPE 1: %s\nTYPE 2: %s\n"
		"ARG 1: %s\nARG 2: %s\nOFFSET: %i\nSIGNED: %u\n"
		"IMMEDIATE: %lx\n",
		op, cc, type1, type2, arg1, arg2, in->off, in->sign,
		in->imm);
	fflush(stderr);
}

void dump_machine(struct machine* m)
{
	if (!m) {
		return;
	}
	fprintf(stderr,
		"R0:  %016lx | R1:  %016lx\n"
		"R2:  %016lx | R3:  %016lx\n"
		"R4:  %016lx | R5:  %016lx\n"
		"R6:  %016lx | R7:  %016lx\n"
		"R8:  %016lx | R9:  %016lx\n"
		"R10: %016lx | R11: %016lx\n"
		"R12: %016lx | R13: %016lx\n"
		"RIP: %016lx | RSP: %016lx\n",
		m->reg[0], m->reg[1], m->reg[2], m->reg[3], m->reg[4],
		m->reg[5], m->reg[6], m->reg[7], m->reg[8], m->reg[9],
		m->reg[10], m->reg[11], m->reg[12], m->reg[13],
		m->reg[RIP], m->reg[RSP]);
	fflush(stderr);
}

