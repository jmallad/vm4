#if (unix || __linux__)
#define _POSIX_C_SOURCE 200112L
#include <time.h>
#elif (_WIN32)
#include <synchapi.h>
#else
#error Unsupported platform!
#endif

void nsleep(long ns) {
#if (__linux__ || unix)
	struct timespec ts = {0, ns};
	nanosleep(&ts, NULL);
#elif (_WIN32)
	Sleep(ns / 1000000);	
#endif
}
