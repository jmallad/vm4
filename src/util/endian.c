#include <string.h>
#include "types.h"

#if __GNUC__
__attribute__ ((const))
#endif
/* Returns 1 on a big-endian system, 0 on a little-endian system
   Behavior on any other type of system is undefined */
int endian_test ()
{
	union {
		u32 i;
		u8 c[4];
	} e = { 0x01000000 };
	if (e.c[0]) {
		return 1;
	}
	return 0;
}
/* The following three functions byte-swap types between endian orders */
void order16(void* in)
{
	u16* p = in;
	u8 t;
	union {
		u16 u16;
		u8 u8[2];
	} x;
	x.u16 = *p;
	t = x.u8[0];
	x.u8[0] = x.u8[1];
	x.u8[1] = t;
	memcpy(in, &x, 2);
}
void order32(void* in)
{
	u32* p = in;
	u8 r[4];
	u8 a;
	u8 b;
	union {
		u32 u32;
		u8 u8[4];
	} x;
	x.u32 = *p;
	b = 3;
	for (a = 0; a < 4; a++) {
		r[a] = x.u8[b];
		b--;
	}
	memcpy(in, r, 4);
}
void order64(void* in)
{
	u64* p = in;
	u8 r[8];
	u8 a;
	u8 b;
	union {
		u64 u64;
		u8 u8[8];
	} x;
	x.u64 = *p;
	b = 7;
	for (a = 0; a < 8; a++) {
		r[a] = x.u8[b];
		b--;
	}
	memcpy(in, r, 8);
}

