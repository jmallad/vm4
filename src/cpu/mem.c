#include <stdlib.h>
#include <string.h>
#include "cpu/machine.h"
#include "types.h"
#include "cpu/mem.h"
#include "cpu/interrupts.h"
#include "cpu/timers.h"

/* Resolve the physical address of a virtually-mapped register */
static void* map_reg(struct machine* m, int* out, u64 in)
{
	unsigned idx;
	idx = (in / 8);
	if (in % 8) {
		/* Unaligned accesses are invalid */
		*out = EINVADDR;
		return NULL;
	}
	*out = SUCCESS;
	return &m->reg[idx];
}

/* Resolve the physical address of a virtually-mapped special register */
static void* map_sreg(struct machine* m, int* out, u64 in)
{
	*out = SUCCESS;
	switch (in) {
	case T0B:
		return &m->tv0;
	case T1B:
		return &m->tv1;
	case ICRB:
		return &m->icr;
	case TCRB:
		return &m->tcr;
	case CPUB:
		return &m->freq;
	case UKMB:
		return &m->ukm;
	case TABLEB:
		return &m->table;
	}
	*out = EINVADDR;
	return NULL;
}

/* Resolve the physical address of a linear virtual address */
static void* linear_addr(struct machine* m, int* out, u64 in)
{
	u32 frame;
	unsigned off;
	/* Calculate page index and allocate it if needed */
	frame = in / m->memory.pagelen;
	if (frame > m->memory.npages) {
		*out = EINVADDR;
		return NULL;
	}
	if (!m->memory.ram[frame]) {
		if (m->memory.nalloc >= m->memory.maxalloc) {
			goto ealloc;
		}
		m->memory.ram[frame] = malloc(m->memory.pagelen);
		if (!m->memory.ram[frame]) {
			goto ealloc;
		}
		memset(m->memory.ram[frame], 0, m->memory.pagelen);
		m->memory.nalloc++;
	}
	/* Calculate offset into frame */
	off = in % m->memory.pagelen;
	*out = SUCCESS;
	/* Validate address */
	if (!frame) {
		if (off < IOB) {
			*out = EINVADDR;
			return NULL;
		}
	}
	return &m->memory.ram[frame][off];
ealloc:
	*out = EALLOC;
	return NULL;
}

/* Retrieve the frame number for a paged virtual address as according to
   the currently configured Page Table */
static int get_frame(struct machine* m, u32* out, u64 in)
{
	u32 page;
	if (m->table < DATAB) {
		return EINVADDR;
	}
	page = in / m->memory.pagelen;
	if (page > m->memory.npages) {
		return EINVADDR;
	}

	return vmemcpy(m, m->table+(PTBLB+(page * 4)), out, 4, TO_HOST,
		       LINEAR);
}

static void* paged_addr(struct machine* m, int* out, u64 in)
{
	unsigned off;
	u32 frame;
	u64 virt;

	/* Retrieve the frame number */
	*out = get_frame(m, &frame, in);
	if (*out) {
		return NULL;
	}
	if (!frame) {
		*out = EPFAULT;
		return NULL;
	}
	/* Calculate offset into frame */
	off = in % m->memory.pagelen;	
	/* Calculate linear address */
	virt = (frame * m->memory.pagelen) + off;
	if (virt < DATAB) {
		*out = EGPROT;
		return NULL;
	}
	return linear_addr(m, out, virt);
}

static void* phys_addr(struct machine* m, int* out, u64 in, u8 mode)
{
	if (!m) {
		*out = ENULLPTR;
		return NULL;
	}
	/* Verify address is within valid range */
	if (in > m->memory.aslen - 1) {
		*out = EINVADDR;
		return NULL;
	}
	/* Check if address refers to a mapped general-purpose register */
	if (in <= REGMAPE) {
		return map_reg(m, out, in);
	}
	/* Check if address refers to a mapped special register */
	if (in <= SREGMAPE) {
		if (mode == AUTO && m->ukm != KERNEL) {
			*out = EGPROT;
			return NULL;
		}
		return map_sreg(m, out, in);
	}
	/* Allocate the page table if needed */
	if (!m->memory.ram) {
		m->memory.ram = malloc(sizeof(u8*)*m->memory.npages);
		if (!m->memory.ram) {
			*out = EALLOC;
			return NULL;
		}
		memset(m->memory.ram, 0, sizeof(u8*)*m->memory.npages);
	}
	switch(mode) {
		/* Guest interface */
	case AUTO:
		/* Kernel-level access */	
		if (m->ukm == KERNEL) {
			return linear_addr(m, out, in);
		}
		/* User-level access */
		return paged_addr(m, out, in);
	case LINEAR:
		return linear_addr(m, out, in);
	case PAGED:
		return paged_addr(m, out, in);
	}
	*out = EINVARG;
	return NULL;
}

int mem_check(struct machine* m, u64 virt, u8 mode)
{
	u32 mapbyte;
	u8 page;
	u8 data;
	int ret;
	if (!m) {
		return ENULLPTR;
	}
	if (mode >= MODE_MAX) {
		return EINVINST;
	}
	if (m->ukm == KERNEL) {
		return SUCCESS;
	}
	if (virt < REGMAPE) {
		return SUCCESS;
	}
	if (m->table < DATAB) {
		return EINVADDR;
	}
	page = virt / m->memory.pagelen;
	mapbyte = MPMAPB + ((page * MODE_MAX) / 8);
	if (mapbyte > MPMAPE) {
		return EINVADDR;
	}
	ret = vmemcpy(m, m->table+mapbyte, &data, 1, TO_HOST, LINEAR);
	if (ret) {
		return ret;
	}
	if (data & (1 << mode)) {
		return SUCCESS;
	}
	return EGPROT;
}

/* when dir is nonzero, direction is from host to the VM */
static int memcpy_crosspage(struct machine* m, u64 virt, void* phys,
			    size_t len, u8 dir, u8 mode)
{
	int ret;
	unsigned w;     /* Number of bytes to write in one loop iteration */
	u8* dst = NULL;
	u8* src = NULL;
	unsigned t = 0; /* Total bytes written */

	/* When copying to virtual memory, the source address never needs to
	   change. */
	if (dir == TO_VM) {
		src = phys;
	}
	/* The inverse condition is true when copying to physical memory */
	else {
		dst = phys;
	}
	while (t < len) {
		if (dir == TO_VM) {
			dst = phys_addr(m, &ret, virt, mode);
			if (ret) {
				return ret;
			}
			if (mode == AUTO) {
				ret = mem_check(m, virt, MODE_WRITE);
				if (ret) {
					return ret;
				}
			}
		}
		/* When copying from virtual memory, the source address must
		   be resolved for each page. */
		else {
			src = phys_addr(m, &ret, virt, mode);
			if (ret) {
				return ret;
			}
			if (mode == AUTO) {
				ret = mem_check(m, virt, MODE_READ);
				if (ret) {
					return ret;
				}
			}
		}
		/* Calculate the number of bytes which can fit into the area
		   starting at virt */
		w = virt - ((virt / m->memory.pagelen) *
			    (m->memory.pagelen % virt));
		/* when w is equal to m->memory.pagelen, the above equation 
		   equals zero - otherwise, it is equivalent to the number of 
		   bytes that can fit into this area. */
		if (w != m->memory.pagelen) {
			w = m->memory.pagelen - w;
		}
		/* when w is larger than the total amount of requested copy
		   bytes, reduce it by the amount of bytes already copied. */
		if (w > len - t) {
			w = len - t;
		}
		/* When copying to virtual memory, we index into the source
		   address as the destination address is resolved on each
		   iteration of the loop */
		if (dir == TO_VM) {
			memcpy(dst, &src[t], w);
		}
		/* We index into the destination address here for the opposite
		   of the reason explained above */
		else {
			memcpy(&dst[t], src, w);
		}
		virt += w;
		t += w;
	}
	return SUCCESS;
}

int vmemcpy(struct machine* m, u64 virt, void* phys1, size_t len, u8 dir,
	    u8 mode)
{
	int ret;
	u8* phys2;
	unsigned i1;
	unsigned i2;
	if (!m || !phys1) {
		return ENULLPTR;
	}
	if (virt+len > m->memory.aslen-1) {
		return EINVADDR;
	}
	/* Calculate the highest and lowest page numbers responsible for
	   the requested virtual area. If they are the same, a copy is
	   fast and simple. Else, we hand off the job to memcpy_crosspage */
	i1 = virt / m->memory.pagelen;
	i2 = (virt + len) / m->memory.pagelen;
	if (i1 == i2) {
		phys2 = phys_addr(m, &ret, virt, mode);
		if (ret) {
			return ret;
		}
		if (dir) {
			if (mode == AUTO) {
				ret = mem_check(m, virt, MODE_WRITE);
				if (ret) {
					return ret;
				}
			}
			memcpy(phys2, phys1, len);
		}
		else {
			if (mode == AUTO) {
				ret = mem_check(m, virt, MODE_READ);
				if (ret) {
					return ret;
				}
			}
			memcpy(phys1, phys2, len);
		}
		return SUCCESS;
	}
	return memcpy_crosspage(m, virt, phys1, len, dir, mode);
}

