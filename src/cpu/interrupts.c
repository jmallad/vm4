#include "types.h"
#include "util/ringbuf.h"
#include "cpu/mem.h"
#include "cpu/interrupts.h"
#include "cpu/instruction.h"
#include "cpu/stack.h"

static int interrupt_try(struct machine* m, int id, int err)
{
	int ret;
	u32 ivec;
	ret = vmemcpy(m, IVTB+(id*4), &ivec, 4, TO_HOST, AUTO);
	if (ret) {
		return ret;
	}
	if (!ivec) {
		return err;
	}
	ret = interrupt_push(m, id);
	if (ret) {
		return ret;
	}
	return SUCCESS;
}

/* Translate a host error code to an interrupt vector. Check if this vector is 
   unmasked and set. Trigger the interrupt if so. Returns SUCCESS if the
   exception was successfully passed to the machine. */
int error_handle(struct machine* m, int err)
{
	switch(err) {
	case ENULLPTR:
		return interrupt_try(m, err, INULL);
	case EINVINST:
	case EINVTARGET:
	case EINVSIZE:
	case EINVARG:
	case EINVINT:
		return interrupt_try(m, err, IINST);
	case EINVSP:
	case EINVADDR:
	case ENOTFOUND:
		return interrupt_try(m, err, IADDR);
	case EDIVZERO:
		return interrupt_try(m, err, IDIVZ);
	case EGPROT:
		return interrupt_try(m, err, IGPROT);
	case EPFAULT:
		return interrupt_try(m, err, IPFAULT);
	}
	return err;
}

int interrupt_push(struct machine* m, u8 id)
{
	if (!m) {
		return ENULLPTR;
	}
	if (!m->intbuf) {
		return ENULLPTR;
	}
	if (id >= IMAX) {
		return EINVINT;
	}
	return ringbuf_push(m->intbuf, id);
}

/* Attempt to handle a single interrupt */
int interrupt_handle(struct machine* m)
{
	int ret;
	u8 id;
	u32 ivec;
	if (!m) {
		return ENULLPTR;
	}
	if (m->state == INTERRUPTED ||
	    !(m->nints)) {
		return SUCCESS;
	}
	if (m->reg[RSP] < DATAB) {
		return EINVSP;
	}
	/* Pop an interrupt from the queue */
	ret = ringbuf_pop(m->intbuf, &id);
	if (ret) {
		return ret;
	}
	if (id >= IMAX) {
		return EINVINT;
	}
	/* This interrupt is maskable and masked, ignore it and return 
	   successfully */
	if (id <= 17 && m->icr & (1 << id)) {
		return SUCCESS;
	}
	ret = vmemcpy(m, IVTB+(id*4), &ivec, 1, TO_HOST, AUTO);
	if (ret) {
		return ret;
	}
	/* No handler currently defined for this unmasked vector */
	if (!ivec) {
		return EINVINT;
	}
	/* Save the current machine context */
	ret = machine_save(m);
	if (ret) {
		return ret;
	}
	/* During a syscall or T0 interrupt, enter kernel mode */
	if ((id == IT0 || id == ISYS) && m->ukm != KERNEL) {
		/* It is the programs responsiblity to switch stacks when
		   necessary */
		m->ukm = KERNEL;
	}
	/* Jump to the interrupt vector */
	m->reg[RIP] = ivec;
	m->nints--;
	m->state = INTERRUPTED;
	return SUCCESS;
}

