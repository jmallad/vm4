#include "types.h"
#include "cpu/machine.h"
#include "cpu/timers.h"
#include "cpu/mem.h"
#include "util/ringbuf.h"
#include "cpu/interrupts.h"

/* Enable/disable timers */
int timer_enable(struct machine* m, u8 id)
{
	if (!m) {
		return ENULLPTR;
	}
	if (id >= TMAX) {
		return EINVINT;
	}
	m->tcr |= (1 << id);
	return SUCCESS;
}

int timer_disable(struct machine* m, u8 id)
{
	if (!m) {
		return ENULLPTR;
	}
	if (id >= TMAX) {
		return EINVINT;
	}
	m->tcr &= ~(1 << id);
	return SUCCESS;
}

/* Check if timer `id' needs triggering and push interrupt if so */
static int timer_check(struct machine* m, u8 id)
{
	int ret;
	u64* tv = &m->tv0;
	u8 ivec = IT0;

	if (id >= TMAX) {
		return EINVINT;
	}
	if (id) {
		tv = &m->tv1;
		ivec = IT1;
	}
	if (!(m->tcr & (1 << id))) {
		return SUCCESS;
	}
	if (!(m->step % *tv)) {
		ret = interrupt_push(m, ivec);
		if (ret) {
			return ret;
		}
		m->nints++;
	}
	return SUCCESS;
}

/* Check both timers 0 and 1 */
int timer_checks(struct machine* m)
{
	int ret;
	ret = timer_check(m, 0);
	if (ret) {
		return ret;
	}
	return timer_check(m, 1);
}

