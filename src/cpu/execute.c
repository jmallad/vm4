#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "cpu/instruction.h"
#include "cpu/machine.h"
#include "cpu/mem.h"
#include "cpu/xint.h"
#include "cpu/stack.h"
#include "cpu/interrupts.h"
#include "util/endian.h"
#include "stream/io.h"

static int load_arg(struct machine* m, xint* out, struct instruction* in,
		    unsigned argn)
{
	u8 type;
	u64 arg;
	u64 off;
	if (!m || !out || !in) {
		return ENULLPTR;
	}
	switch (argn) {
	case 1:
		type = in->arg1_type;
		arg = in->arg1;
		break;
	case 2:
		type = in->arg2_type;
		arg = in->arg2;
		break;
	default:
		return EINVARG;
	}
	out->size = in->size;
	switch(type) {
	case REG:
		return store_xint(out, &m->reg[arg], in->size, m->swap);
	case MEM:
		return vstore_xint(m, out, arg, in->size);
	case PTR:
		return vstore_xint(m, out, m->reg[arg]+in->off, in->size);
	case IMM:
		return store_xint(out, &in->imm, in->size, m->swap);
	case OFF:
		off = m->reg[arg];
		off += in->off;
		return store_xint(out, &off, in->size, m->swap);
	}
	return EINVTARGET;
}

static int store_arg(struct machine* m, xint* in, struct instruction* instr,
		     unsigned argn)
{
	u8 type;
	u64 arg;
	if (!m || !instr || !in) {
		return ENULLPTR;
	}
	switch (argn) {
	case 1:
		type = instr->arg1_type;
		arg = instr->arg1;
		break;
	case 2:
		type = instr->arg2_type;
		arg = instr->arg2;
		break;
	default:
		return EINVARG;
	}
	switch (type) {
	case REG:
		return load_xint(&m->reg[arg], in, m->swap);
	case MEM:
		return vload_xint(m, arg, in);
	case PTR:
		return vload_xint(m, m->reg[arg]+instr->off, in);
	}
	return EINVTARGET;		       
}

static int compare_lg(u8 result, u8 sign, u8 icon, u8 ucon)
{
	if (sign) {
		if (result & (1 << icon)) {
			return 1;
		}
		return 0;
	}
	if (result & (1 << ucon)) {
		return 1;
	}
	return 0;
}

static int compare_lge(u8 result, u8 sign, u8 icon, u8 ucon)
{
	if (result & (1 << EF)) {
		return 1;
	}
	return compare_lg(result, sign, icon, ucon);
}

static int compare(struct machine* m, struct instruction* in)
{
	switch(in->cc) {
	case NC:
		return 1;
	case ZC:
		return (m->result & (1 << ZF));
	case EC:
		return (m->result & (1 << EF));
	case NE:
		return (m->result & (1 << NF));
	case LC:
		return compare_lg(m->result, in->sign, LF, BF);
	case GC:
		return compare_lg(m->result, in->sign, GF, AF);
	case LEC:
		return compare_lge(m->result, in->sign, LF, BF);
	case GEC:
		return compare_lge(m->result, in->sign, GF, AF);
	}
	return 0;
}

int execute_math_two(struct machine* m, struct instruction* in)
{
	int ret;
	xint x = {0};
	xint y = {0};
	if (!compare(m, in)) {
		return SUCCESS;
	}
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	ret = load_arg(m, &y, in, 2);
	if (ret) {
		return ret;
	}
	ret = op_xint(&x, &y, in->op, in->sign);
	if (ret) {
		return ret;
	}
	return store_arg(m, &x, in, 1);
}

int execute_math_one(struct machine* m, struct instruction* in)
{
	int ret;
	xint x = {0};
	if (!compare(m, in)) {
		return SUCCESS;
	}
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	ret = op_xint(&x, NULL, in->op, in->sign);
	if (ret) {
		return ret;
	}
	return store_arg(m, &x, in, 1);
}

int execute_mem_two(struct machine* m, struct instruction* in)
{
	int ret;
	xint x = {0};
	if (!compare(m, in)) {
		return SUCCESS;
	}
	ret = load_arg(m, &x, in, 2);
	if (ret) {
		return ret;
	}
	ret = store_arg(m, &x, in, 1);
	return ret;
}

int execute_cmp_two(struct machine* m, struct instruction* in)
{
	xint x = {0};
	xint y = {0};
	int ret;
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	ret = load_arg(m, &y, in, 2);
	if (ret) {
		return ret;
	}
	ret = compare_xint(&x, &y, in->sign);
	if (ret < 0) {
		return ret;
	}
	m->result = ret;
	return SUCCESS;
}

int execute_cmp_one(struct machine* m, struct instruction* in)
{
	xint x = {0};
	int ret;
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	ret = compare_xint(&x, NULL, in->sign);
	if (ret < 0) {
		return ret;
	}
	m->result = ret;
	return SUCCESS;
}

int execute_call_one(struct machine* m, struct instruction* in)
{
	xint x = {0};
	u64 ip;
	int ret;
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	ip = m->reg[RIP];
	ret = machine_push(m, &ip, 8);
	if (ret) {
		return ret;
	}
	m->reg[RIP] = x.x64;
	return SUCCESS;
}

int execute_ret_one(struct machine* m, struct instruction* in)
{
	xint x = {0};
	u64 ip;
	u64 sp;
	int ret;
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	/* Release any allocated stack space as indicated */
	sp = m->reg[RSP];
	if (x.x64) {
		if (sp > ((m->memory.aslen - 1) - x.x64) ||
		    sp + x.x64 < DATAB) {
			return EINVSP;
		}
		sp += x.x64;
		m->reg[RSP] = sp;
	}
	/* A set sign bit specifies that this is an interrupt return */
	if (in->sign) {
		m->state = RUNNING;
		/* Restore the context of the machine before interruption.
		   It is the programs responsibility to leave kernel mode when
		   desired. */
		ret = machine_restore(m);
		if (ret) {
			return ret;
		}
		return SUCCESS;
	}
	/* Regular function return */
	ret = machine_pop(m, &ip, 8);
	if (ret) {
		return ret;
	}
	m->reg[RIP] = ip;
	return SUCCESS;
}

int execute_push_one(struct machine* m, struct instruction* in)
{
	int ret;
	xint x = {0};
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	return machine_push(m, &x.x64, x.size);
}

int execute_pop_one(struct machine* m, struct instruction* in)
{
	int ret;
	xint x = {0};
	ret = machine_pop(m, &x.x64, in->size);
	if (ret) {
		return ret;
	}
	return store_arg(m, &x, in, 1);	
}

int execute_int_one(struct machine* m, struct instruction* in)
{
	int ret;
	xint x = {0};
	
	ret = load_arg(m, &x, in, 1);
	if (ret) {
		return ret;
	}
	return interrupt_push(m, x.x8);
}


