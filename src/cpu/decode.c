#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "cpu/cpu.h"
#include "cpu/instruction.h"
#include "cpu/execute.h"
#include "util/endian.h"

static u8 resolve_size(i8 in)
{
	switch(in) {
	case 0:
		return 1;
	case 1:
		return 2;
	case 2:
		return 4;
	case 3:
		return 8;
	}
	return 0;
}

static int decode_upper(struct machine* m, u64* out, struct instruction* in)
{
	int ret = SUCCESS;
	switch(in->size) {
	case 1:
		ret = fetch(m, out, 1);
		break;
	case 2:
		ret = fetch(m, out, 2);
		break;
	case 4:
		ret = fetch(m, out, 4);
		break;
	case 8:
		ret = fetch(m, out, 8);
	}
	return ret;
}

static int decode_args(struct instruction* out)
{
	switch(out->arg1_type) {
	case NONE:
		return SUCCESS;
	case REG:
	case PTR:
		out->arg1 = out->r1;
		break;
	case MEM:
		out->arg1 = out->imm;
		break;
	default:
		return EINVARG;
	}
	switch(out->arg2_type) {
	case NONE:
		break;
	case REG:
	case PTR:
	case OFF:
		out->arg2 = out->r2;
		break;
	case MEM:
	case IMM:
		out->arg2 = out->imm;
		break;
	default:
		return EINVARG;
	}
	return SUCCESS;		
}

int decode(struct machine* m, executor* e, struct instruction* out,
		  u64 in)
{
	int ret;
	u64 upper = 0;
	if (!out) {
		return ENULLPTR;
	}
	out->op   = in & OP_MASK;
	out->cc   = (in & CC_MASK)   >> 5;
	out->r1   = (in & R1_MASK)   >> 8;
	out->r2   = (in & R2_MASK)   >> 12;
	out->sign = (in & SIGN_MASK) >> 16;
	out->size = (in & SIZE_MASK) >> 17;
	out->off  = (in & OFF_MASK)  >> 19;
	out->arg1_type = (in & DST_MASK) >> 35;
	out->arg2_type = (in & SRC_MASK) >> 37;
	out->size = resolve_size(out->size);
	if (!out->size) {
		return EINVSIZE;
	}
	if (out->arg1_type == MEM ||
	    out->arg2_type == MEM ||
	    out->arg2_type == IMM) {
		ret = decode_upper(m, &upper, out);
		if (ret) {
			return ret;
		}
		out->imm = ((in & UMASK(24, 64)) >> 40) | (upper & LMASK(40));
	}
	if (m->swap) {
		order16(&out->off);		
	}
	ret = decode_args(out);
	if (ret) {
		return ret;
	}
	switch(out->op) {
	case ADD:
	case SUB:
	case MUL:
	case DIV:
	case MOD:
	case SHB:
	case AND:
	case OR:
	case XOR:
	case ROB:
	case FADD:
	case FSUB:
	case FMUL:
	case FDIV:
	case FCHS:
		*e = execute_math_two;
		break;
	case NEG:
	case NOT:
		*e = execute_math_one;
		break;
	case MOV:
		*e = execute_mem_two;
		break;
	case CMP:
		if (out->arg2_type) {
			*e = execute_cmp_two;
		}
		else {
			*e = execute_cmp_one;
		}
		break;
	case CALL:
		*e = execute_call_one;
		break;
	case RET:
		*e = execute_ret_one;
		break;
	case PUSH:
		*e = execute_push_one;
		break;
	case POP:
		*e = execute_pop_one;
		break;
	case INT:
		*e = execute_int_one;
		break;
	}
	return SUCCESS;
}

