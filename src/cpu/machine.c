#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "types.h"
#include "cpu/cpu.h"
#include "cpu/stack.h"
#include "cpu/mem.h"
#include "cpu/interrupts.h"
#include "util/debug.h"
#include "util/ringbuf.h"
#include "util/nsleep.h"
#include "util/endian.h"

const char* vm_strerror(int ret)
{
	switch (ret) {
	case LIMITED:
		return "limited";
	case HALTED:
		return "halted";
	case SUCCESS:
		return "success";
	case ENULLPTR:
		return "null pointer dereference";
	case EALLOC:
		return "dynamic allocation failure";
	case EINVINST:
		return "invalid instruction";
	case EINVTARGET:
		return "invalid target";
	case EINVSIZE:
		return "invalid size";
	case EINVARG:
		return "invalid argument number";
	case EINVADDR:
		return "invalid address";
	case ENOTFOUND:
		return "page not found";
	case EREAD:
		return "Failed to read file";
	}
	return strerror(ret);
}

int machine_run_nofreq(struct machine* m)
{
	int ret;
	m->state = RUNNING;
	while (1) {
		if (m->debug) {
			dump_machine(m);
		}
		ret = step(m);
		if (ret) {
			ret = error_handle(m, ret);
			if (ret) {
				break;
			}
		}
	} 
	if (m->debug) {
		dump_machine(m);
	}
	m->state = STOPPED;
	return ret;
}

int machine_run(struct machine* m)
{
	int ret;
	u64 delta;
	struct timespec start = {0};
	struct timespec end = {0};;
	m->state = RUNNING;
	while (1) {
		if (m->debug) {
			dump_machine(m);
		}
		if (!timespec_get(&start, TIME_UTC)) {
			ret = ETIMESPEC;
			break;
		}
		ret = step(m);
		if (ret) {
			ret = error_handle(m, ret);
			if (ret) {
				break;
			}
		}
		if (!timespec_get(&end, TIME_UTC)) {
			ret = ETIMESPEC;
			break;
		}

		delta = m->freq - (end.tv_nsec - start.tv_nsec);
		if (delta <= m->freq) {
			nsleep(delta);
			continue;
		}
		nsleep(m->freq);
	}
	if (m->debug) {
		dump_machine(m);
	}
	m->state = STOPPED;
	return ret;
}

int machine_new(struct machine** out)
{
	int ret;
	struct machine* m;
	m = malloc(sizeof(struct machine));
	if (!m) {
		return EALLOC;
	}
	memset(m, 0, sizeof(struct machine));
	m->reg[RIP] = 8;
	/* Allocate interrupt queue */	
	ret = ringbuf_new(&m->intbuf, DEFAULT_INTQUEUE, 1);
	if (ret) {
		machine_free(m);
		return ret;
	}
	/* Configure virtual memory with default properties */
	m->memory.aslen = DEFAULT_ASLEN;
	m->memory.pagelen = DEFAULT_PAGELEN;
	m->memory.npages = DEFAULT_NPAGES;
	m->memory.maxalloc = DEFAULT_MAXALLOC;
	*out = m;
	return SUCCESS;
}

int machine_load(struct machine* out, void* in, u64 inlen, u8 endian,
		 u64 load, u64 start)
{
	int ret;
	if (!out || !in) {
		return ENULLPTR;
	}
	if (!inlen) {
		return SUCCESS;
	}
	if (endian != endian_test()) {
		out->swap = 1;
		order64(&inlen);
		order64(&load);
		order64(&start);
	}
	ret = vmemcpy(out, load, in, inlen, TO_VM, LINEAR);
	if (ret) {
		return ret;
	}
	out->reg[RIP] = start;
	/* Start in kernel mode with default parameters */
	out->ukm = KERNEL;
	out->table = 0;
	/* Start with timers and I/O interrupts disabled and masked */
	out->icr = 0x3ffff;
	out->tcr = 0;
	return SUCCESS;
}

void machine_free(struct machine* m)
{
	unsigned i;
	if (!m) {
		return;
	}
	if (m->memory.ram) {
		for (i = 0; i < m->memory.npages; i++) {
			if (m->memory.ram[i]) {
				free(m->memory.ram[i]);
			}
		}
		free(m->memory.ram);
	}
	if (m->intbuf) {
		ringbuf_free(m->intbuf);
	}
	free(m);
}

/* Context save/restore */

int machine_save(struct machine* m)
{
	int ret;
	ret = machine_push(m, &m->reg, sizeof(u64)*16);
	if (ret) {
		return ret;
	}
	return machine_push(m, &m->result, 1);
}

int machine_restore(struct machine* m)
{
	int ret;
	ret = machine_pop(m, &m->result, 1);
	if (ret) {
		return ret;
	}
	return machine_pop(m, &m->reg, sizeof(u64)*16);
}

/* Setters */

void machine_setlimit(struct machine* m, size_t limit)
{
	if (!m) {
		return;
	}
	m->limit = limit;
}

void machine_setfreq(struct machine* m, u64 hz)
{
	if (!m) {
		return;
	}
	if (!hz) {
		return;
	}
	m->freq = (1000000000 / hz);
}

void machine_setmaxalloc(struct machine* m, unsigned maxalloc)
{
	if (!m) {
		return;
	}
	m->memory.maxalloc = maxalloc;
}

int machine_setmaxints(struct machine* m, u8 maxints)
{
	if (!m) {
		return ENULLPTR;
	}
	m->maxints = maxints;
	if (m->intbuf) {
	        ringbuf_free(m->intbuf);		
	}
	return ringbuf_new(&m->intbuf, maxints, 1);
}

void machine_setdebug(struct machine* m, u8 debug)
{
	if (!m) {
		return;
	}
	m->debug = debug;
}

void machine_setmode(struct machine* m, u8 mode)
{
	if (!m) {
		return;
	}
	if (mode >= MODEMAX) {
		return;
	}
	m->ukm = mode;
}

int machine_getstate(struct machine* m)
{
	if (!m) {
		return ENULLPTR;
	}
	return m->state;
}
