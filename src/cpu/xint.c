#include <math.h>
#include <string.h>
#include "types.h"
#include "util/endian.h"
#include "cpu/machine.h"
#include "cpu/instruction.h"
#include "cpu/xint.h"
#include "cpu/mem.h"
#include "rotate/rotate.h"

int valid_size(i8 in)
{
	switch(in) {
	case 1:
	case 2:
	case 4:
	case 8:
		return 1;
	}
	return 0;
}
static void iadd_xint(xint* x, xint* y)
{
	x->s64 += y->s64;
}
static void isub_xint(xint* x, xint* y)
{
	x->s64 -= y->s64;
}
static void imul_xint(xint* x, xint* y)
{
	x->s64 *= y->s64;
}
static int idiv_xint(xint* x, xint* y)
{
	if (!y->s64) {
		return EDIVZERO;
	}
	x->s64 /= y->s64;
	return SUCCESS;
}
static int imod_xint(xint* x, xint* y)
{
	if (!x->s64) {
		return EDIVZERO;
	}
	x->s64 %= y->s64;
	return SUCCESS;
}
static void add_xint(xint* x, xint* y)
{
	x->x64 += y->x64; 
}
static void sub_xint(xint* x, xint* y)
{
	x->x64 -= y->x64; 
}
static void mul_xint(xint* x, xint* y)
{
	x->x64 *= y->x64; 
}
static int div_xint(xint* x, xint* y)
{
	if (!y->x64) {
		return EDIVZERO;
	}
	if (y->x64) {
		x->x64 /= y->x64;
	}
	return SUCCESS;
}
static int mod_xint(xint* x, xint* y)
{
	if (!x->x64) {
		return EDIVZERO;
	}
	x->x64 %= y->x64;
	return SUCCESS;
}
static void shr_xint(xint* x, xint* y)
{
	x->x64 >>= y->x64; 
}
static void shl_xint(xint* x, xint* y)
{
	x->x64 <<= y->x64; 
}
static void ror_xint(xint* x, xint* y)
{
	switch(x->size) {
	case 1:
		x->x8 = ror8_native(x->x8, y->x8);
		break;
	case 2:
		x->x16 = ror16_native(x->x16, y->x16);
		break;
	case 4:
		x->x32 = ror32_native(x->x32, y->x32);
		break;
	case 8:
		x->x64 = ror64_native(x->x64, y->x64);
		break;
	}
}
static void rol_xint(xint* x, xint* y)
{
	switch(x->size) {
	case 1:
		x->x8 = ror8_native(x->x8, (8 - y->x8));
		break;
	case 2:
		x->x16 = ror16_native(x->x16, (16 - y->x16));
		break;
	case 4:
		x->x32 = ror32_native(x->x32, (32 - y->x32));
		break;
	case 8:
		x->x64 = ror64_native(x->x64, (64 - y->x64));
		break;
	}
}
static void and_xint(xint* x, xint* y)
{
	x->x64 &= y->x64; 
}
static void or_xint(xint* x, xint* y)
{
	x->x64 |= y->x64; 
}
static void xor_xint(xint* x, xint* y)
{ 
	x->x64 ^= y->x64; 
}
static void fadd_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		x->f32 += y->f32;
	}
	x->f64 += y->f64;
}
static void fiadd_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		x->f32 += y->s64;
		return;
	}
	x->f64 += y->s64;
}
static void fsub_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		x->f32 -= y->f32;
	}
	x->f64 -= y->f64;
}
static void fisub_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		x->f32 -= y->s64;
		return;
	}
}
static void fmul_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		x->f32 *= y->f32;
	}
	x->f64 *= y->f64;
}
static void fimul_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		x->f32 *= y->s64;
		return;
	}
	x->f64 *= y->s64;
}
static int fdiv_xint(xint* x, xint* y)
{
	if (!y->x64) {
		return EDIVZERO;
	}
	if (x->size == 4) {
		x->f32 /= y->f32;
	}
	x->f64 /= y->s64;
	return SUCCESS;
}
static int fidiv_xint(xint* x, xint* y)
{
	if (!y->x64) {
		return EDIVZERO;
	}
	if (x->size == 4) {
		x->f32 /= y->x64;
		return SUCCESS;
	}
	x->f64 /= y->x64;
	return SUCCESS;
}
static int fmod_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		if (!x->f32) {
			return EDIVZERO;
		}
		x->f32 = remainderf(x->f32, y->f32);
	}
	if (!x->f64) {
		return EDIVZERO;
	}
	x->f64 = remainder(x->f64, y->f64);
	return SUCCESS;
}
static int fimod_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		if (!x->f32) {
			return EDIVZERO;
		}
		x->f32 = remainderf(x->f32, (float)y->s32);
		return SUCCESS;
	}
	if (!x->f64) {
		return EDIVZERO;
	}
	x->f64 = remainder(x->f64, (double)y->s64);
	return SUCCESS;
}
static void fchs_xint(xint* x, xint* y)
{
	if (x->size == 4) {
		x->f32 = -(y->f32);
		return;
	}
	x->f64 = -(y->f64);
}
static void neg_xint(xint* x)
{
	x->x64 = -(i64)x->x64;
}
static void not_xint(xint* x)
{
	x->x64 = ~x->x64;
}

static int binary_compare_int(u64 x, u64 y, u8 sign)
{
	int result = 0;
	if (x == y) {
		result |= (1 << EF);
		return result;
	}
	result |= (1 << NF);
	if (sign) {
		if ((i64)x > (i64)y) {
			result |= (1 << NF);
		}
		if ((i64)x < (i64)y) {
			result |= (1 << LF);
		}
		return result;
	}
	if (x > y) {
		result |= (1 << AF);
	}
	if (x < y) {
		result |= (1 << BF);
	}
	return result;
}

static int unary_compare_int(u64 x)
{
	if (x) {
		return 0;
	}
	return 0 | (1 << ZF);
}

int compare_xint(xint* x, xint* y, u8 sign)
{
	if (!y) {
		return unary_compare_int(x->x64);
	}
	return binary_compare_int(x->x64, y->x64, sign);
}

static int unary_xint(xint* x, u8 op)
{
	switch(op) {
	case NEG:
		neg_xint(x);
		break;
	case NOT:
		not_xint(x);
		break;
	default:
		return EINVINST;		
	}
	return SUCCESS;
}

int op_xint(xint* x, xint* y, u8 op, u8 sign)
{
	int ret;
	if (!y) {
		return unary_xint(x, op);
	}
	switch(op) {
	case ADD:
		if (sign) {
			iadd_xint(x, y);
			break;
		}
		add_xint(x, y);
		break;
	case SUB:
		if (sign) {
			isub_xint(x, y);
			break;
		}
		sub_xint(x, y);
		break;
	case MUL:
		if (sign) {
			imul_xint(x, y);
			break;
		}
		mul_xint(x, y);
		break;
	case DIV:
		if (sign) {
			ret = idiv_xint(x, y);
		}
		else {
			ret = div_xint(x, y);
		}
		if (ret) {
			return ret;
		}
		break;
	case MOD:
		if (sign) {
			ret = imod_xint(x, y);
		}
		else {
			ret = mod_xint(x, y);
		}
		if (ret) {
			return ret;
		}
		break;
	case SHB:
		if (sign) {
			shl_xint(x, y);
		}
		else {
			shr_xint(x, y);
		}
		break;
	case AND:
		and_xint(x, y);
		break;
	case OR:
		or_xint(x, y);
		break;
	case XOR:
		xor_xint(x, y);
		break;
	case ROB:
		if (sign) {
			rol_xint(x, y);
			break;
		}
		ror_xint(x, y);
		break;
	case FADD:
		if (sign) {
			fiadd_xint(x, y);
			break;
		}
		fadd_xint(x, y);
		break;
	case FSUB:
		if (sign) {
			fisub_xint(x, y);
			break;
		}
		fsub_xint(x, y);
		break;
	case FMUL:
		if (sign) {
			fimul_xint(x, y);
			break;
		}
		fmul_xint(x, y);
		break;
	case FDIV:
		if (sign) {
			ret = fidiv_xint(x, y);
			if (ret) {
				return ret;
			}
			break;
		}
		ret = fdiv_xint(x, y);
		if (ret) {
			return ret;
		}
		break;
	case FMOD:
		if (sign) {
			ret = fimod_xint(x, y);
			if (ret) {
				return ret;
			}
			break;
		}
		ret = fmod_xint(x, y);
		if (ret) {
			return ret;
		}
		break;
	case FCHS:
		fchs_xint(x, y);
		break;
	default:
		return EINVINST;
	}
	return SUCCESS;
}

void swap_xint(xint* in)
{
	switch (in->size) {
	case 1:
		return;
	case 2:
		order16(&in->x16);
		return;
	case 4:
		order32(&in->x32);
		return;
	case 8:
		order64(&in->x64);
		return;
	}	
}

int store_xint(xint* out, void* in, i8 size, bool swap)
{
	if (!out || !in) {
		return ENULLPTR;
	}
	if (!valid_size(size)) {
		return EINVSIZE;
	}
	out->size = size;
	memcpy(&out->x64, in, size);
	if (swap) {
		swap_xint(out);
	}
	return SUCCESS;
}

int load_xint(void* out, xint* in, bool swap)
{
	if (!in) {
		return 0;
	}
	if (!valid_size(in->size)) {	       
		return EINVSIZE;
	}
	if (swap) {
		swap_xint(in);
	}
	memcpy(out, &in->x64, in->size);
	return SUCCESS;
}

int vload_xint(struct machine* m, u32 out, xint* in)
{
	if (!in || !out) {
		return 0;
	}
	if (!valid_size(in->size)) {
		return EINVSIZE;
	}
	if (m->swap) {
		swap_xint(in);
	}
	return vmemcpy(m, out, &in->x64, in->size, TO_VM, AUTO);
}

int vstore_xint(struct machine* m, xint* out, u32 in, i8 size)
{
	int ret;
	if (!m || !out) {
		return 0;
	}
	if (!valid_size(size)) {
		return EINVSIZE;
	}
	ret = vmemcpy(m, in, &out->x64, size, TO_HOST, AUTO);
	if (m->swap) {
		swap_xint(out);
	}
	return ret;
}
