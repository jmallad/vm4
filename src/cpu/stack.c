#include "cpu/stack.h"
#include "types.h"
#include "cpu/machine.h"
#include "cpu/instruction.h"
#include "cpu/xint.h"
#include "cpu/mem.h"

int machine_push(struct machine* m, void* in, size_t size)
{
	if (!m || !in) {
		return ENULLPTR;
	}
	if (valid_size(size)) {
		return EINVSIZE;
	}
	if (m->reg[RSP] - size < DATAB) {
		return EINVSP;
	}
	m->reg[RSP] -= size;
	return vmemcpy(m, m->reg[RSP], in, size, TO_VM, AUTO);
}

int machine_pop(struct machine* m, void* out, size_t size)
{
	int ret;
	if (!m || !out) {
		return ENULLPTR;
	}
	if (valid_size(size)) {
		return EINVSIZE;
	}
	if (m->reg[RSP] + size > m->memory.aslen - 1) {
		return EINVSP;
	}
	ret = vmemcpy(m, m->reg[RSP], out, size, TO_HOST, AUTO);
	if (ret) {
		return ret;
	}
	m->reg[RSP] += size;
	return SUCCESS;
}


