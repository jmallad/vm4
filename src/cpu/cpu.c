#include "types.h"
#include "cpu/machine.h"
#include "cpu/mem.h"
#include "cpu/interrupts.h"
#include "cpu/timers.h"
#include "cpu/instruction.h"
#include "cpu/execute.h"
#include "cpu/decode.h"
#include "util/debug.h"

/* Fetch instruction data to pass to the decoder */
int fetch(struct machine* m, void* out, size_t len)
{
	u64 ip;
	int ret;
	if (!m || !out) {
		return ENULLPTR;
	}
	ip = m->reg[RIP];
	if (ip + len > m->memory.aslen - 1 || ip < DATAB) {
		return EINVADDR;
	}
	if (!ip) {
		return HALTED;
	}
	ret = mem_check(m, ip, MODE_EXECUTE);
	if (ret) {
		return ret;
	}
	ret = vmemcpy(m, ip, out, len, TO_HOST, AUTO);
	if (ret) {
		return ret;
	}
        m->reg[RIP] += len;
	return SUCCESS;
}

int step(struct machine* m)
{
	int ret;
	executor exec;
	u64 packed = 0;
	struct instruction instr = {0};
	if (m->limit) {
		if (m->step >= m->limit) {
			return LIMITED;
		}
	}
	if (m->state == STOPPED) {
		return HALTED;
	}
	ret = timer_checks(m);
	if (ret) {
		return ret;
	}
	ret = interrupt_handle(m);
	if (ret) {
		return ret;
	}
	ret = fetch(m, &packed, 5);
	if (ret) {
		return ret;
	}
	if (!packed) {
		return HALTED;
	}
	ret = decode(m, &exec, &instr, packed);
	if (m->debug) {
		dump_instr(&instr);
	}
	if (ret) {
		return ret;
	}
	if (!exec) {
		return ENULLPTR;
	}
	ret = exec(m, &instr);
	if (ret) {
		return ret;
	}
	m->step++;
	return SUCCESS;
}
