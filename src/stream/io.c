#include "types.h"
#include "cpu/machine.h"
#include "cpu/xint.h"
#include "cpu/interrupts.h"

int io_write(struct machine* m, u8 out, void* in, u16 size)
{
	int ret;
	u16 rp;
	u16 wp;
	u16 w;
	u16 t;
	u8* src;
	if (!m || !in) {
		return ENULLPTR;
	}
	if (out > 15) {
		return EINVCHAN;
	}
	/* Grab the current read and write pointers */
	ret = vmemcpy(m, IOCB+(IOCBLEN*out)+2, &wp, 2, TO_HOST, LINEAR);
	if (ret) {
		return ret;
	}
	ret = vmemcpy(m, IOCB+(IOCBLEN*out), &rp, 2, TO_HOST, LINEAR);
	if (ret) {
		return ret;
	}
	/* Determine if we need to write one chunk or two */
	if (wp < rp) {
		w = wp - rp;
		t = w;
	}
	else {
		w = wp + (u16)IOBLEN-wp;
		t = w + rp;
	}
	if (w == 1 || size < t) {
		return EFULL;
	}
	if (w > size) {
		w = size;
	}      
	/* Write the first chunk */
	ret = vmemcpy(m, IOB+(IOBLEN*out)+wp, in, w, TO_VM, LINEAR);
	if (ret) {
		return ret;
	}
	wp += w;
	/* Write the second chunk if present */
	if (w < size) {
		wp = 0;
		src = in;
		ret = vmemcpy(m, IOB+(IOBLEN*out)+wp, &src[w], size - w,
			      TO_VM, LINEAR);
		if (ret) {
			return ret;
		}
		wp = size - w;
		w += wp;
	}
	/* Save the new write pointer */
	ret = vmemcpy(m, IOCB+(IOCBLEN*out)+2, &wp, 2, TO_VM, LINEAR);
	if (ret) {
		return ret;
	}
	/* Trigger the relevant channel interrupt */
	ret = interrupt_push(m, out);
	if (ret) {
		return ret;
	}
	return w;
}

int io_read(struct machine* m, u8 in, void* out, u16 size)
{
	int ret;
	u16 wp;
	u16 rp;
	u16 r;
	u16 t;
	u8* dst;
	if (!m || !out) {
		return ENULLPTR;
	}
	if (in > 15) {
		return EINVCHAN;
	}
	if (!size) {
		return SUCCESS;
	}
	/* Grab the current read and write pointers */
	ret = vmemcpy(m, IOCB+(IOCBLEN*in), &rp, 2, TO_HOST, LINEAR);
	if (ret) {
		return ret;
	}
	ret = vmemcpy(m, IOCB+(IOCBLEN*in)+2, &wp, 2, TO_HOST, LINEAR);
	if (ret) {
		return ret;
	}
	/* Determine if we need to read one chunk or two */
	if (wp < rp) {
		r = rp + (u16)IOBLEN-rp;
		t = r + wp;
	}
	else {
		r = wp - rp;
		t = r;
	}
	if (r == 1 || size < t) {
		return EEMPTY;
	}
	if (r > size) {
		r = size;
	}
	/* Read the first chunk */
	ret = vmemcpy(m, IOB+(IOBLEN*in)+rp, out, r, TO_HOST, LINEAR);
	if (ret) {
		return ret;
	}
	rp += r;
	/* Read the second chunk if present */
	if (r < size) {
		rp = 0;
		dst = out;
		ret = vmemcpy(m, IOB+(IOBLEN*in), &dst[r], size - r, TO_VM, LINEAR);
		if (ret) {
			return ret;
		}
		rp = size - r;
		r += rp;
	}
	/* Save the new read pointer */
	ret = vmemcpy(m, IOCB+(IOCBLEN*in), &rp, 2, TO_VM, LINEAR);
	if (ret) {
		return ret;
	}
	return r;
}

