#+OPTIONS: html-style:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="docstyle.css" />
*Copyright 2021 Joshua Mallad <joshua@mallad.org>. All rights reserved.*

* API Error Handling

** Exit status / error code interpretation

*** Overview
Most API calls return the `int' type. Negative values indicate error. Negative
values less than zero (0) indicates success [SUCCESS]. One (1) [HALTED] is
returned by =machine_run()= to indicate that a program terminated normally.
A small number of functions use positive values as normal return values. Error
codes can be interpreted via the following API call:

*** API call which returns a human-readable description of an error code
#+BEGIN_SRC C
const char* vm_strerror(int err);
#+END_SRC

This will return a pointer to a static string that describes the
error. Do not modify or free this buffer. 

*** Full list of status codes 

|------------+-------+------------------------------------------------------|
| Pnemonic   | Value | Description                                          |
| LIMITED    |     2 | Machine instruction counter hit the configured limit |
| HALTED     |     1 | Normal program termination                           |
| SUCCESS    |     0 | No error, function returned successfully             |
| ENULLPTR   |    -1 | Null pointer dereference                             |
| EALLOC     |    -2 | Dynamic allocation failure                           |
| EINVINST   |    -3 | Invalid instruction                                  |
| EINVTARGET |    -4 | Invalid target                                       |
| EINVSIZE   |    -5 | Invalid size specified                               |
| EINVARG    |    -6 | Invalid argument number specified                    |
| EINVADDR   |    -7 | Invalid address specified                            |
| ENOTFOUND  |    -8 | Page not found                                       |
| EREAD      |   -11 | Failed to read file                                  |
| EASM       |   -12 | Invalid assembly                                     |
| EINVSTREAM |   -13 | Invalid stream type                                  |
| EMODE      |   -14 | I/O permission failure                               |
| EDIVZERO   |   -15 | Division by zero                                     |
| EBUFOV     |   -16 | Buffer overflow                                      |
| EINVSP     |   -17 | Invalid stack pointer                                |
| EEMPTY     |   -18 | Buffer empty                                         |
| EFULL      |   -19 | Buffer full                                          |
| ETIMESPEC  |   -20 | Time library error                                   |
| EINVINT    |   -21 | Invalid interrupt vector                             |
| EINVCHAN   |   -22 | Invalid I/O channel specified                        |
| EGPROT     |   -23 | General protection fault                             |
| EPFAULT    |   -24 | Page fault                                           |


