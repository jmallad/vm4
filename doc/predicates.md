;; Predicate definitions for  vm4
;; Copyright (C) 2021 Joshua Mallad

(define_predicate "imm29_operand"
  (ior (and (match_code "const_int")
	    (match test "IN_RANGE (INTVAL (op), 0, 536870911)"))
       (match_code "symbol_ref,label_ref,const")))

(define predicate "dst_binary_operand"
  (ior
   (match_operand 0 "register_operand")
   (match_operand 0 "memory_operand")
   ))

(define predicate "src_binary_operand"
  (ior
   (match_operand 1 "register_operand")
   (match_operand 1 "imm29_operand")
   (match_operand 1 "memory_operand")
   ))

(define predicate "alu_unary_operand"
  (ior
   (match_operand 0 "register_operand")
   (match_operand 0 "memory_operand")
   ))

(define predicate "mem_unary_operand"
  (ior
   (match_operand 0 "alu_one_operand")
   (match_operand 0 "imm29_operand")
   ))






    
