u64 splitmix64(u64* state)
{
	u64 z;
	(*state) += 0x9e3779b97f4a7c15;
	z = *state;
        z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;  
	z = (z ^ (z >> 27)) * 0x94d049bb133111eb; 
	return z ^ (z >> 31);                     
}

void generate(u64* out, unsigned max, u64* state)
{
	unsigned i;

	for (i = 0; i < max; i++) {
		out[i] = splitmix64(state);
	}
}

int main()
{
	u64 state = 0x7ad567ea;
	u64 results[8] = {0};
	generate(results, 8, &state);
	return 0;
}

