;; u64 splitmix64(u64* state) 
splitmix64:
        ;; (*state) += 0x9e3779b97f4a7c15 
        add [r0], 0x9e3779b97f4a7c15
	;; z = *state 
	mov r1, [r0]
	;; z >> 30 
	mov r2, r1
	shr r2, 30
	;; z ^ (z >> 30) 
	mov r3, r1
	xor r3, r2
	;; z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9 
	mul r3, 0xbf58476d1ce4e5b9
	;; z >> 27 
	mov r1, r3
	shr r1, 27
	;; z ^ (z >> 27) 
	mov r2, r3
	xor r2, r1
	;; z = (z ^ (z >> 27)) * 0x94d049bb133111eb 
	mul r2, 0x94d049bb133111eb
	;; return z ^ (z >> 31) 
	mov r1, r2
	shr r1, 31
	mov r0, r2
	xor r0, r1
	ret

;; void generate(u64* out, unsigned max, u64* state) 
generate:
	;; i = 0 
	mov r3, 0
	;; if (!max) { break } 
	cmp r1, 0
	je break
loop:
	;; save registers 
	push r0			
	push r1
	push r3			
	;; splitmix64(state) 
	mov r0, 24 		; 24 is mapped address of r2
	call splitmix64
	mov r4, r0
	;; restore registers
	pop r3			
	pop r1
	pop r0
	;; out[i] =
	push r5			
	mov r5, 8
	mul r5, r3
	add r0, r5
	mov [r0], r4
	pop r5
	;; i++
	add r3, 1
	;; if (i < max) { goto loop } 
	cmp r3, r1
	jb loop
break:	
	ret

;; int main()
main:
	;; u64 state = 0x7ad567ea
	mov r5, 0x7ad567ea
	;; u64 results[8] = {0}
	sub rsp, 64
	mov r0, rsp
	mov r1, 8
	mov r2, 48		; mapped r5
	call generate
	;; return 0
	mov r0, 0
	ret
